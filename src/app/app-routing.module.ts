import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './main/core/home/home.component';

const routes: Routes = [
  {
    path: 'users',
    loadChildren: './main/core/users/user.module#UserModule'
  },
  {
    path: '',
    redirectTo: '',
    pathMatch: 'full',
    component: HomeComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
  declarations: [HomeComponent],
  providers: []
})
export class AppRoutingModule { }
