export class User {
    id: number;
    firstName: string;
    lastName: string;
    companyName: string;
    city: string;
    state: string;
    zip: number;
    email: string;
    web: string;
    age: number;
}
