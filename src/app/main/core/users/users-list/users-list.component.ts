import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { User } from './user.model';
import { PaginationService } from 'src/app/main/shared/services/pagination.service';

@Component({
  selector: 'app-users-list',
  templateUrl: './users-list.component.html',
  styleUrls: ['./users-list.component.scss']
})
export class UsersListComponent implements OnInit {

  public searchStr = '';
  public pager: any = {};
  public pagedItems: User[];
  public searchedItems: User[];
  private isAscending = false;
  private usersList: User[];

  constructor(
    private userService: UserService,
    private pagerService: PaginationService
  ) { }

  ngOnInit() {
    this.getUsers();
  }

  /**
   * @description this function set the users list
   * @author Varun Kumar
   */
  getUsers() {
    this.userService.getUsers().subscribe(
      res => {
        if (res) {
          this.usersList = res;
          this.userService.setUsersList(res);
          this.setPage(1);
        }
      },
      err => console.log(err)
    );
  }

  /**
   * @description this function arrange the users in ascending and descending order in the table
   * @param {string}
   * @returns {object[]}
   * @author Varun Kumar
   */
  sortTable(prop) {
    if (this.isAscending) {
      this.pagedItems.sort((a, b) => {
        return a[prop] === b[prop] ? 0 : +(a[prop] > b[prop]) || -1;
      });
    } else {
      this.pagedItems.sort((a, b) => {
        return a[prop] === b[prop] ? 0 : +(a[prop] < b[prop]) || -1;
      });
    }
    this.isAscending = !this.isAscending;
  }

  /**
   * @description this function set the number of users on current page
   * @param {number}
   * @author Varun Kumar
   */
  setPage(page: number) {
    if (!this.searchStr.trim()) {
      this.pager = this.pagerService.getPager(this.usersList.length, page);
      this.pagedItems = this.usersList.slice(this.pager.startIndex, this.pager.endIndex + 1);
    } else {
      this.setSearchPages(page);
    }
  }

  /**
   * @description this function set the number of pages on the pagination bar according to the search string
   * @param {number}
   * @author Varun Kumar
   */
  setSearchPages(page: number) {
    this.pager = this.pagerService.getPager(this.searchedItems.length, page);
    this.pagedItems = this.searchedItems.slice(this.pager.startIndex, this.pager.endIndex + 1);
  }

  /**
   * @description this function used to apply the search operation on the table
   * @param {string}
   * @author Varun Kumar
   */
  search(term: string) {
    this.searchedItems = JSON.parse(JSON.stringify(this.usersList.filter( x => x.firstName.trim().toLowerCase().includes(term))));
    if (!term) {
      this.pagedItems = JSON.parse(JSON.stringify(this.usersList));
      this.setPage(1);
    } else {
      this.pagedItems = this.usersList.filter(x =>
        x.firstName.trim().toLowerCase().includes(term.trim().toLowerCase())
      );
      this.setSearchPages(1);
    }
  }
}
