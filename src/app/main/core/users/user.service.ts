import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { User } from './users-list/user.model';
import { map } from 'rxjs/operators';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private corsUrl = 'https://cors.mentorz.com:9000/cors-escape/';
  private url = `${this.corsUrl}https://datapeace-storage.s3-us-west-2.amazonaws.com/dummy_data/users.json`;
  private userListSubject = new BehaviorSubject(null);
  public userList = this.userListSubject.asObservable();

  constructor(
    private httpClient: HttpClient
  ) { }

  /**
   * @description this function return the all users
   * @returns {Observable}
   * @author Varun Kumar
   */
  getUsers() {
    return this.httpClient.get(this.url).pipe(map((data: any[]) => data.map((item: any) =>  {
      const model = new User();
      Object.assign(model, item);
      model.firstName = item.first_name;
      model.lastName = item.last_name;
      model.companyName = item.company_name;
      delete model['first_name'];
      delete model['last_name'];
      delete model['company_name'];
      return model;
    })));
  }

  /**
   * @description this function set the user list
   * @author Varun Kumar
   */
  setUsersList(data) {
    this.userListSubject.next(data);
  }

}
