import { Component, OnInit } from '@angular/core';
import { UserService } from '../user.service';
import { User } from '../users-list/user.model';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-users-details',
  templateUrl: './users-details.component.html',
  styleUrls: ['./users-details.component.scss']
})
export class UsersDetailsComponent implements OnInit {

  public userDetails: User;
  private userList: User[] = [];

  constructor(
    private userService: UserService,
    private route: ActivatedRoute
  ) { }

  ngOnInit() {
    this.userService.userList.subscribe(res => {
      if (res) {
        this.userList = res;
        this.getParam();
      } else {
        this.setUsers();
      }
    });
  }

  /**
   * @description this function set the users list
   * @author Varun Kumar
   */
  setUsers() {
    this.userService.getUsers().subscribe(
      res => {
        if (res) {
          this.userList = res;
          this.userService.setUsersList(res);
          this.getParam();
        }
      },
      err => console.log(err)
    );
  }

  /**
   * @description this function receives the id from url params
   * @author Varun Kumar
   */
  getParam() {
    this.route.params.subscribe(params => {
      const id = params['id'];
      this.setUserDetails(id);
    });
  }

  /**
   * @description this function set the user details
   * @author Varun Kumar
   */
  setUserDetails(id: number) {
    this.userDetails = this.getUser(id);
  }

  /**
   * @description this function filter the current user details form users list
   * @returns {object}
   * @author Varun Kumar
   */
  getUser(id: number) {
    const user: any = this.userList.filter((u: User) => {
      if (u.id === +id) {
        return u;
      }
    });
    return user[0];
  }
}
