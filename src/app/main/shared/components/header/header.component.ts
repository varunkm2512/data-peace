import { Component, OnInit } from '@angular/core';
import { Router, NavigationEnd } from '@angular/router';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  public isHomePage = true;
  public isUserListPage = false;
  public isUserDetailsPage = false;

  constructor(
    private router: Router
  ) {
    router.events.forEach((event) => {
      if (event instanceof NavigationEnd) {
        const url: string = event.url;
        if (url.includes('users')) {
          if (url.includes('detail')) {
            this.isHomePage = false;
            this.isUserListPage = false;
            this.isUserDetailsPage = true;
          } else {
            this.isHomePage = false;
            this.isUserListPage = true;
            this.isUserDetailsPage = false;
          }
        } else {
          this.isHomePage = true;
          this.isUserListPage = false;
          this.isUserDetailsPage = false;
        }
      }
    });
  }

  ngOnInit() {
  }
}
