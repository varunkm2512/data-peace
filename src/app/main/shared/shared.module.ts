import { NgModule } from '@angular/core';
import { FilterPipe } from './pipes/filter.pipe';
import { HeaderComponent } from './components/header/header.component';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

@NgModule({
  declarations: [FilterPipe, HeaderComponent],
  imports: [
    CommonModule,
    RouterModule
  ],
  exports: [FilterPipe, HeaderComponent]
})
export class SharedModule { }
